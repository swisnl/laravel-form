# 2.0.0

* Cleanup of model names (were plural)
* Added more structure helpers

# 1.4.1

* Collection::lists was deprecated and is now removed. Use "::pluck" instead.
* Dont list static methods as actions
* Add Viewcomposer for all layouts in configured path

# 1.4.0

* Query scope for online models
* Fixed bug where Head-title that was already set, was overwritten
