<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 24-10-2017
 * Time: 14:50
 */

namespace LaravelForm\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use LaravelForm\Models\Form\Interfaces\Form;
use LaravelForm\SanitizeService;

class Autoreply extends Mailable
{
    protected $formValues;
    use Queueable;

    public $form;
    protected $sanitizeService;

    public function __construct(Form $form, SanitizeService $sanitizeService)
    {
        $this->form = $form;
        $this->sanitizeService = $sanitizeService;
    }

    public function build()
    {
        $this->formValues = $this->sanitizeService->sanitizeArray($this->form->getValues());

        return $this->from($this->form->afzender_email, $this->form->afzender_naam)
            ->subject($this->form->autoreply_onderwerp)
            ->view('laravel-form::mail.autoreply')
            ->with(
                [
                    'title' => $this->form->titel,
                    'body' => $this->body(),
                ]
            );
    }

    protected function body()
    {
        $body = $this->form->autoreply_tekst;

        collect($this->formValues)->each(
            function ($value, $key) use (&$body) {
                if(is_array($value)){
                    $value = implode(', ', $value);
                }
                $body = str_replace('{'.$key.'}', $value, $body);
            }
        );

        $body = $this->replaceFormulierTitel($body);
        $body = $this->replaceFormulierVelden($body);

        return $body;
    }

    public function replaceFormulierTitel($body)
    {
        return str_replace('{formuliertitel}', $this->form->titel, $body);
    }

    public function replaceFormulierVelden($body)
    {
        $formValues = '<table>';
        foreach ($values = $this->sanitizeService->sanitizeArray($this->form->getValues()) as $key => $value)
        {
            $formValues .= '<tr><td> '.$key.' </td><td> ' . $value . ' </td></tr>';
        }
        $formValues .= '</table>';

        return str_replace('{formuliervelden}', $formValues, $body);
    }
}