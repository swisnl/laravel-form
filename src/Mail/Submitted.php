<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 24-10-2017
 * Time: 14:50
 */

namespace LaravelForm\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use LaravelForm\Models\Form\Interfaces\Form;
use LaravelForm\SanitizeService;
use SimpleXMLElement;

class Submitted extends Mailable
{
    use Queueable;

    public $form;
    protected $sanitizeService;
    protected $xml;

    public function __construct(Form $form, SanitizeService $sanitizeService)
    {
        $this->form = $form;
        $this->sanitizeService = $sanitizeService;
    }

    public function build()
    {
        $this->formValues = $this->sanitizeService->sanitizeArray($this->form->getValues());
        $this->xml = $this->array_to_xml($this->formValues, new SimpleXMLElement('<form-values/>'));

        return $this->from($this->form->afzender_email, $this->form->afzender_naam)
            ->subject('Gevuld formulier: ' . $this->form->titel)
            ->attachData($this->xml->asXML(), 'form-values.xml', [
                'mime' => 'text/xml',
            ])
            ->view('laravel-form::mail.submitted')
            ->with(
                [
                    'title' => $this->form->titel,
                    'body' => $this->body(),
                ]
            );
    }

    protected function body()
    {
        $body = $this->form->autoreply_tekst;
        $values = $this->sanitizeService->sanitizeArray($this->form->getValues());

        $formValues = '<table>';

        collect($values)->each(
            function ($value, $key) use (&$body, &$formValues) {
                if(is_array($value)){
                    $value = implode(', ', $value);
                }
                $body = str_replace('{'.$key.'}', $value, $body);
                $formValues .= '<tr><td>'.$key.'</td><td>' . $value . '</td></tr>';
            }
        );

        $formValues .= '</table>';

        $body = $this->replaceFormulierTitel($body);
        $body = $this->replaceFormulierVelden($body);

        return $body . $formValues;
    }

    public function array_to_xml($arr, SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            is_array($v)
                ? $this->array_to_xml($v, $xml->addChild($k))
                : $xml->addChild($k, $v);
        }

        return $xml;
    }

    public function replaceFormulierTitel($body)
    {
        return str_replace('{formuliertitel}', $this->form->titel, $body);
    }

    public function replaceFormulierVelden($body)
    {
        $formValues = '<table>';
        foreach ($values = $this->sanitizeService->sanitizeArray($this->form->getValues()) as $key => $value)
        {
            $formValues .= '<tr><td> '.$key.' </td><td> ' . $value . ' </td></tr>';
        }
        $formValues .= '</table>';

        return str_replace('{formuliervelden}', $formValues, $body);
    }
}