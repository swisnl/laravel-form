<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 14:59
 */

namespace LaravelForm\Models\Form;


use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{

    protected $table = 'formulier_veld';

    public function form()
    {
        return $this->belongsTo(Form::class, 'formulier_id');
    }

    public function options()
    {
        return $this->hasMany(FormFieldOption::class, 'veld_id');
    }

    public function getValidationAttribute(){
        if($this->required){
            return 'required|' . $this->validator;
        }
        return $this->validator;
    }
}