<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 14:59
 */

namespace LaravelForm\Models\Form;


use Illuminate\Database\Eloquent\Model;
use LaravelForm\SanitizeService;

class FormResult extends Model
{
    protected $sanitizeService;
    protected $table = 'formulier_resultaat';

    /**
     * FormResult constructor.
     */
    public function __construct()
    {
        $this->sanitizeService = new SanitizeService();

        parent::__construct();
    }

    public function form()
    {
        return $this->belongsTo(Form::class, 'formulier_id');
    }

    public function setFormDataAttribute(array $requestData)
    {
        $values = $this->sanitizeService->sanitizeArray($requestData);

        $this->attributes['form_data'] = json_encode($values);
    }

    /**
     * @param SanitizeService $sanitizeService
     */
    public function setSanitizeService(SanitizeService $sanitizeService)
    {
        $this->sanitizeService = $sanitizeService;
    }


}