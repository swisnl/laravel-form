<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:34
 */

namespace LaravelForm\Models\Form\Elements;

use LaravelForm\Models\Form\Interfaces\Element;
use LaravelForm\Models\Form\Traits\HasOptions;

abstract class AbstractElement implements Element
{
    /** @var  string */
    protected $name;

    protected $attributes = [];

    /** @var  string */
    protected $label;

    /** @var string  */
    protected $type = 'text';

    /** @var bool|string */
    protected $rules = false;

    /** @var mixed */
    protected $value;

    protected $required = false;

    protected $hasOptions = false;

    /**
     * AbstractElement constructor.
     * @param $label
     * @param $name
     * @param bool|string $rules
     */
    public function __construct($label, $name, $rules = false)
    {
        $this->setName($name);
        $this->setLabel($label);

        if($rules){
            $this->setRules($rules);
        }

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setRules(string $rules)
    {
        $this->rules = $rules;
        return $this;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function render($closure = false)
    {
        if($closure){
            return $closure;
        }

        $data = [];
        $data['name'] = $this->getName();
        $data['type'] = $this->getType();
        $data['label'] = $this->getLabel();
        $data['attributes'] = $this->getAttributes();
        $data['required'] = (bool) $this->required;

        if(in_array(HasOptions::class, class_uses($this))){
            $data['options'] = $this->getOptions();
        }

        return view('laravel-form::element.'.$this->getType(), $data);
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValueForSaving() {
        return $this->getValue();
    }

    public function getValuesForDisplaying() {
        return $this->getValue();
    }

    public function __toString()
    {
        return (string) $this->render();
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     */
    public function setRequired(bool $required)
    {
        $this->required = $required;
    }

}