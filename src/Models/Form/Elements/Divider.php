<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:09
 */

namespace LaravelForm\Models\Form\Elements;
use LaravelForm\Models\Form\Interfaces\Element;

class Divider extends AbstractElement implements Element
{
    /** @var string  */
    protected $type = 'divider';

}