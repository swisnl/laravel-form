<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:09
 */

namespace LaravelForm\Models\Form\Elements;
use LaravelForm\Models\Form\Interfaces\Element;
use LaravelForm\Models\Form\Traits\HasOptions;

class Select extends AbstractElement implements Element
{
    use HasOptions;

    /** @var string  */
    protected $type = 'select';

}