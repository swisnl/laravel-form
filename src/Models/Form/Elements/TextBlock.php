<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:09
 */

namespace LaravelForm\Models\Form\Elements;
use LaravelForm\Models\Form\Interfaces\Element;

class TextBlock extends AbstractElement implements Element
{
    /** @var string  */
    protected $type = 'textblock';

    /** @var string */
    protected $text;

    public function render($closure = false)
    {
        if($closure){
            return $closure;
        }

        return view('laravel-form::element.'.$this->getType(), ['text' => $this->getText()]);
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

}