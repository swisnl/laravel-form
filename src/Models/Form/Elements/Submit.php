<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:09
 */

namespace LaravelForm\Models\Form\Elements;
use LaravelForm\Models\Form\Interfaces\Element;

class Submit extends AbstractElement implements Element
{
    /** @var string  */
    protected $type = 'submit';

    protected $buttonText = 'Submit';

    public function __construct($buttonText)
    {
        $this->setButtonText($buttonText);
        parent::__construct('submit', 'submit');
    }

    /**
     * @return string
     */
    public function getButtonText(): string
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     */
    public function setButtonText(string $buttonText)
    {
        $this->buttonText = $buttonText;
    }

    public function render($closure = false)
    {
        if($closure){
            return $closure;
        }

        return view('laravel-form::element.'.$this->getType(),
            [
                'name' => $this->getName(),
                'type' => $this->getType(),
                'buttonText' => $this->getButtonText(),
            ]);
    }


}