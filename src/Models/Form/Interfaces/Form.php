<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:06
 */

namespace LaravelForm\Models\Form\Interfaces;

use LaravelForm\Models\Form\Elements\AbstractElement;

interface Form
{
    public function getName();
    public function setName($name);
    public function getMethod();
    public function setMethod($method);
    public function getAction();
    public function setAction(string $action);

    public function getElements();
    public function addElement(AbstractElement $element);

    public function setRules(array $rules);
    public function getRules();

    public function setValues(array $values);
    public function getValues();

    public function sendsAutoreply();
    public function autoReplyAddress();

    public function render();
    public function __toString();

    public function handle();
}