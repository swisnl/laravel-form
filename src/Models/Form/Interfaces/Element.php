<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:08
 */

namespace LaravelForm\Models\Form\Interfaces;


interface Element
{
    public function getName();
    public function setName(string $name);

    public function getLabel();
    public function setLabel(string $name);

    public function getType();

    public function setRules(string $rules);
    public function getRules();

    public function setValue($value);
    public function getValue();

    public function getValueForSaving();
    public function getValuesForDisplaying();

    public function render($closure = false);
    public function __toString();
}