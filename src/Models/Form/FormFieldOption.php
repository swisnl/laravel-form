<?php
/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 14:59
 */

namespace LaravelForm\Models\Form;


use Illuminate\Database\Eloquent\Model;

class FormFieldOption extends Model
{

    protected $table = 'formulier_veld_multi';

    public function field()
    {
        return $this->belongsTo(FormField::class, 'id');
    }
}