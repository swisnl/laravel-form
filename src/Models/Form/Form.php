<?php
namespace LaravelForm\Models\Form;
use Illuminate\Support\Facades\Validator;
use LaravelForm\Models\Form\Elements\AbstractElement;
use LaravelForm\Models\Form\Elements\Checkbox;
use LaravelForm\Models\Form\Elements\Divider;
use LaravelForm\Models\Form\Elements\Radio;
use LaravelForm\Models\Form\Elements\Select;
use LaravelForm\Models\Form\Elements\Submit;
use LaravelForm\Models\Form\Elements\Text;
use LaravelForm\Models\Form\Elements\TextArea;
use LaravelForm\Models\Form\Elements\TextBlock;
use LaravelForm\Models\Form\Interfaces\Element;
use LaravelForm\Models\Form\Interfaces\Form as FormInterface;
use Illuminate\Database\Eloquent\Model;
use WebbeheerLaravel\QueryScopes\WhereOnlineScope;

/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 10:05
 */

class Form extends Model implements FormInterface
{

    /** @var string */
    protected $method = 'post';

    /** @var array Collection */
    protected $elements = [];

    /** @var string */
    protected $action = 'swisform-handle';

    protected $table = 'formulier';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new WhereOnlineScope());
    }

    public function elementModels()
    {
        return $this->hasMany(FormField::class, 'formulier_id')->orderBy('volgorde', 'ASC');
    }

    public function buildElements()
    {
        foreach ($this->elementModels as $model) {

            switch ($model->type_veld) {
                case 'submit':
                    break;
                case 'textArea':
                    $this->elements[$model->name] = new TextArea($model->titel, $model->name, $model->validation);
                    break;
                case 'checkBox':
                    $this->elements[$model->name] = new Checkbox($model->titel, $model->name, $model->validation);
                    $this->elements[$model->name]->setOptions($model->options);
                    break;
                case 'radioButton':
                    $this->elements[$model->name] = new Radio($model->titel, $model->name, $model->validation);
                    $this->elements[$model->name]->setOptions($model->options);
                    break;
                case 'selectField':
                    $this->elements[$model->name] = new Select($model->titel, $model->name, $model->validation);
                    $this->elements[$model->name]->setOptions($model->options);
                    break;
                case 'textblock':
                    $this->elements[$model->name] = new TextBlock($model->titel, $model->name, $model->validation);
                    $this->elements[$model->name]->setText($model->tekst);
                    break;
                case 'hrLine':
                    $this->elements[$model->name] = new Divider($model->titel, $model->name, $model->validation);
                    break;
                default:
                    $this->elements[$model->name] = new Text($model->titel, $model->name, $model->validation);
                    break;
            }

            $this->elements[$model->name]->setRequired((bool) $model->required);
        }

        $this->elements['submit'] = new Submit($this->submit_tekst);
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function customAttributes()
    {
        $attributes = [];

        foreach($this->elementModels as $model) {
            $attributes[$model->name] = $model->titel;

            foreach($model->options as $option) {
                $attributes[$option->optie_key] = $option->optie;
            }
        }

        return $attributes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->titel_key;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function addElement(AbstractElement $element)
    {
        $this->elements[$element->getName()] = $element;
    }

    /**
     * @param array $validationRules
     * @internal param $name
     */
    public function setRules(array $rules)
    {
        foreach ($rules as $name => $rule) {
            $this->elements[$name]->setValidationRule($rule);
        }
    }

    public function render()
    {
        $renderedElements = [];

        /**
         * @var string $name
         * @var Element $element
         */
        foreach ($this->getElements() as $name => $element) {
            $renderedElements[] = $element->render();
        }

        return view('laravel-form::base',
            [
                'elements' => $renderedElements,
                'action' => route($this->getAction()),
                'name' => $this->getName(),
                'method' => $this->getMethod(),
                'id' => $this->id,
                'currentUrl' => request()->url(),
                'title' => $this->titel,
                'show_title' => $this->titel_tonen
            ]);
    }

    public function __toString()
    {
        return (string) $this->render();
    }

    public function handle()
    {
        $this->validate();
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction(string $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        $rules = [];
        /**
         * @var string $name
         * @var Element $element
         */
        foreach ($this->getElements() as $name => $element) {
            if ($element->getRules()) {
                $rules[$name] = $element->getRules();
            }
        }
        return $rules;
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $values = [];
        foreach($this->elements as $name => $element) {
            if ($element instanceof Submit || $element instanceof TextBlock || $element instanceof Divider) {
                continue;
            }

            $name = $this->replaceAttributes($name);

            $value = $element->getValue();
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            $value = $this->replaceAttributes($value);

            $values[$name] = $value;
        }

        return $values;
    }

    private function replaceAttributes($value): string {
        return str_replace(
                array_keys($this->customAttributes()),
                array_values($this->customAttributes()),
                $value);
    }

    /**
     * @param mixed $values
     */
    public function setValues(array $values)
    {
        foreach($this->elements as $name => $element) {
            if(!array_key_exists($name, $values)) {
                continue;
            }

            $this->elements[$name]->setValue($values[$name]);
        }
    }

    public function sendsAutoreply(){
        if((int) $this->autoreply === 1){
            return in_array('required|email', $this->getRules());
        }
        return false;
    }

    public function autoReplyAddress()
    {
        $element = array_search('required|email', $this->getRules());
        return $this->elements[$element]->getValue();
    }
}