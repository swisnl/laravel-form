<?php

namespace LaravelForm\Models\Form\Traits;

/**
 * Created by PhpStorm.
 * User: bvanmoorsel
 * Date: 6-9-2017
 * Time: 15:27
 */

trait HasOptions
{
    /** @var array */
    protected $options = [];

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->hasOptions = true;

        foreach ($options as $option) {
            $this->options[$option['optie_key']] = $option['optie'];
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }


}