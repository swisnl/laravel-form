<?php

namespace LaravelForm;

class SanitizeService {

    /**
     * @param array $requestData
     * @return \Illuminate\Support\Collection
     */
    public function sanitizeArray(array $requestData): \Illuminate\Support\Collection
    {
        $values = collect($requestData);
        $values = $values->map(
            function ($value) {
                if(is_array($value)){
                    return $this->sanitizeArray($value);
                }
                return htmlentities(strip_tags($value));
            }
        );
        return $values;
    }
}