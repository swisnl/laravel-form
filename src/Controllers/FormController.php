<?php

namespace LaravelForm\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\ValidationException;
use LaravelForm\MailerService;
use LaravelForm\Models\Form\Form;
use LaravelForm\Models\Form\FormResult;
use LaravelForm\SanitizeService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FormController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function handle(Request $request, SanitizeService $sanitizeService)
    {
        $returnUrl = $request->get('url', false);
        if ($returnUrl === false || strpos($returnUrl, config('app.url')) === false) {
            throw new BadRequestHttpException("Invalid url in request.");
        }

        /** @var Form $form */
        $form = Form::findOrFail($request->get('id'));
        $form->buildElements();
        try {
            $this->validate($request, $form->getRules(), [], $form->customAttributes());
        } catch(ValidationException $e) {
            // make sure we redirect to the form-page with a hashtag so the page scrolls to the form itself
            $e->redirectTo($returnUrl);
            throw $e;
        }
        $form->setValues($request->all());

        // *save'n
        $result = new FormResult();
        $result->setSanitizeService($sanitizeService);
        $result->setFormDataAttribute($form->getValues());
        $result->form()->associate($form);
        $result->save();

        // mailen
        $mailService = new MailerService($form, $sanitizeService);
        $mailService->sendAutoreply();
        $mailService->sendSubmitted();

        return redirect()->to($returnUrl)->with('form-confirm', true);
    }
}