<?php

namespace LaravelForm;


use LaravelForm\Mail\Autoreply;
use LaravelForm\Mail\Submitted;
use LaravelForm\Models\Form\Interfaces\Form;
use Mail;

class MailerService
{
    protected $form;
    protected $sanitizeService;

    public function __construct(Form $form, SanitizeService $sanitizeService)
    {
        $this->form = $form;

        $this->sanitizeService = $sanitizeService;
    }

    public function sendAutoreply()
    {
        if ($this->form->sendsAutoreply()) {
            Mail::to($this->form->autoReplyAddress())->send(new Autoreply($this->form, $this->sanitizeService));
        }
    }

    public function sendSubmitted()
    {
        Mail::to($this->form->email)->send(new Submitted($this->form, $this->sanitizeService));
    }
}