<?php

namespace LaravelForm\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use LaravelForm\Controllers\FormController;

class LaravelFormServiceProvider extends ServiceProvider
{
    /**
     * Boot structure routing service and create routes to noded.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'laravel-form');

        Route::group(['middleware' => ['web']], function() {
            Route::post('/form-handle', FormController::class . '@handle')->name('swisform-handle');
        });
    }

    /**
     * Load config.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
