<div class="form-group @if($required)required @endif">
    {!! Form::label($name, $label, ['class' => 'control-label']) !!}
    @foreach($options as $key => $option)
        <div class="radio">
            <label>
                {!! Form::radio($name, $key, false, array_merge([], $attributes)) !!} {{ $option }}
            </label>
        </div>
    @endforeach
</div>