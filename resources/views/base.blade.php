<div class="form" id="{{$name}}">
    <div class="row">
        <div class="col-md-8">
            @if($show_title)
                <h1>
                    {{ $title }}
                </h1>
            @endif
            @if (isset($errors) && $errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['url' => $action, 'method' => $method, 'name'=>$name]) !!}
                {!! Form::hidden('id', $id) !!}
                @php
                    $url = \Request::url() . '#' . $name;
                @endphp
                {!! Form::hidden('url', $url) !!}

                @foreach($elements as $element)
                    {!! $element !!}
                @endforeach

            {!! Form::close() !!}
        </div>
    </div>
</div>